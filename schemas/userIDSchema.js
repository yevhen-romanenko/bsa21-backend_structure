const joi = require('joi');

const userID = joi
    .object({
        id: joi.string().uuid(),
    })
    .required();

module.exports = userID;
