const userSchema = require('./userSchema');
const transactionSchema = require('./transactionSchema');
const eventSchema = require('./eventSchema');
const betSchema = require('./betSchema');
const userID = require('./userIDSchema');

const schemas = {
    userSchema,
    transactionSchema,
    eventSchema,
    betSchema,
    userID,
};
module.exports = schemas;
