const dbConfig = require('../knexfile');
const knex = require('knex');

const dbconect = knex(dbConfig.development);

module.exports = dbconect;
