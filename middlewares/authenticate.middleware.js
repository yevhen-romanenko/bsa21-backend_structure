const jwt = require('jsonwebtoken');

const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        try {
            const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
            return tokenPayload;
        } catch (err) {
            return res.status(401).send({ error: 'Not Authorized' });
        }
    }

    next();
};

exports.authenticateJWT = authenticateJWT;
