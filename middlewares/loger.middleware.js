const Logger = (req, res, next) => {
    console.log(`Logged  ${req.originalUrl}  ${req.method} ${res.body} -- ${new Date()}`);

    next();
};

exports.Logger = Logger;
