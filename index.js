const winston = require('winston');
const expressWinston = require('express-winston');
const express = require('express');

const ee = require('events');

const app = express();
app.use(express.json());

const PORT = 3000;

const statEmitter = new ee();

const stats = {
    totalUsers: 3,
    totalBets: 1,
    totalEvents: 1,
};

const routes = require('./routes/index');
routes(app);

app.use(
    expressWinston.logger({
        transports: [new winston.transports.Console()],
        format: winston.format.combine(winston.format.colorize(), winston.format.json()),
        meta: false,
        msg: 'HTTP  ',
        expressFormat: true,
        colorize: false,
        ignoreRoute: function (req, res) {
            return false;
        },
    })
);

app.get('/', (req, res) => {
    res.send('Hello World! - Winston');
});

app.listen(PORT, () => {
    statEmitter.on('newUser', () => {
        stats.totalUsers++;
    });
    statEmitter.on('newBet', () => {
        stats.totalBets++;
    });
    statEmitter.on('newEvent', () => {
        stats.totalEvents++;
    });

    console.log(`App listening at http://localhost:${PORT}`);
});

// Do not change this line
module.exports = { app };
