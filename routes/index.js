const usersRoutes = require('./usersRoutes');
const betsRoutes = require('./betsRoutes');
const eventsRoutes = require('./eventsRoutes');
const statsRoutes = require('./statsRoutes');
const transactionsRoutes = require('./transactionsRoutes');

module.exports = (app) => {
    app.use('/users', usersRoutes);
    app.use('/transactions', transactionsRoutes);
    app.use('/bets', betsRoutes);
    app.use('/events', eventsRoutes);
    app.use('/stats', statsRoutes);
};
