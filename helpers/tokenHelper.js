const jwt = require('jsonwebtoken');

createToken = (data) => jwt.sign(data, process.env.JWT_SECRET);

exports.createToken = createToken;
